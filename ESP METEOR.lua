repeat wait(1) until game:IsLoaded()
local expectedPlaceId = 1224212277
if game.PlaceId ~= expectedPlaceId then
    return
end
game.StarterGui:SetCore("SendNotification", {
Title = "ESP Meteors";
Text = "Ligma",
Icon = "rbxassetid://10678268125";
Duration = 20;
})

-----------10678268125
local expectedPlaceId = 1224212277
if game.PlaceId ~= expectedPlaceId then
    return
end

while wait(2) do
    local piggyEvent = game.workspace.PiggyEvent
    local descendants = piggyEvent:GetDescendants()
    
    for _, v in ipairs(descendants) do
        if v.Name == "Meteor" then
            local a = v:FindFirstChild("GUIesp")
            if not a then
                a = Instance.new("BillboardGui")
                a.Name = "GUIesp"
                a.Size = UDim2.new(10, 0, 10, 0)
                a.AlwaysOnTop = true

                local b = Instance.new("Frame", a)
                b.Size = UDim2.new(1, 0, 1, 0)
                b.BackgroundTransparency = 0.8
                b.BorderSizePixel = 0
                b.BackgroundColor3 = Color3.new(0, 1, 0)

                local c = Instance.new("TextLabel", b)
                c.Size = UDim2.new(2, 0, 2, 0)
                c.BorderSizePixel = 0
                c.TextSize = 20
                c.Text = v.Name
                c.BackgroundTransparency = 1

                a.Parent = v
            end
        end
    end
end
